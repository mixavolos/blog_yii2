<?php

use yii\db\Schema;
use yii\db\Migration;

class m150427_060535_add_rows_lookup extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m150427_060535_add_rows_lookup cannot be reverted.\n";

        return false;
    }*/
    
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert('lookup', [
            'name'=>'Public',
            'code'=>'2',
            'type'=>'postStatus',
            'position'=>'2',
        ]);
        $this->insert('lookup', [
            'name'=>'Arhiv',
            'code'=>'3',
            'type'=>'postStatus',
            'position'=>'3',
        ]);
         $this->insert('lookup', [
            'name'=>'Draft',
            'code'=>'1',
            'type'=>'postStatus',
            'position'=>'1',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('lookup', ['type' => 'postStatus']);
    }
}
