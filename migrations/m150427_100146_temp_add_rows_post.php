<?php

use yii\db\Schema;
use yii\db\Migration;

class m150427_100146_temp_add_rows_post extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m150427_100146_temp_add_rows_post cannot be reverted.\n";

        return false;
    }*/
    
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $i=1;
        while ( $i< 30) {
             $this->insert('post', [
                'title'=>'test_'.$i,
                'text_post'=>'<p>text text'.$i.'</p>',
                'text_prewiev'=>'text',
                'status'=>intval($i/10)+1,
                'tags'=>'test',
                'path_img'=>'/files/images/img'.(intval($i/10)+1).'.jpg',
                'create_time'=>time(),
                'update_time'=>0,
            ]);
            $i++;
        } 
       
    }
    
    public function safeDown()
    {

    }
    
}
