<?php

use yii\db\Schema;
use yii\db\Migration;

class m150426_132155_add_table_lookup extends Migration
{
    /* public function up()
    {

    }

    public function down()
    {
        echo "m150426_132155_add_table_lookup cannot be reverted.\n";

        return false;
    }*/
    
    
    // Use safeUp/safeDown to run migration code within a transaction
   public function safeUp()
    {
        $this->createTable("lookup",array(
            'id'=>'pk',
            'name'=>'VARCHAR(40) NOT NULL',
            'code'=>'INT(10) DEFAULT 0',
            'type'=>'VARCHAR(40) DEFAULT NULL',
            'position'=>'INT(10) DEFAULT 0',
            ));
    }

    public function safeDown()
    {
        $this->dropTable("lookup");
    }
}
