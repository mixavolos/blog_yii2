<?php

use yii\db\Schema;
use yii\db\Migration;

class m150422_204617_add_table_post extends Migration
{
    /*public function up()
    {

    }

    public function down()
    {
        echo "m150422_204617_add_table_post cannot be reverted.\n";

        return false;
    }*/
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable("post",array(
            'id'=>'pk',
            'title'=>'VARCHAR(255) NOT NULL',
            'text_post'=>'text NOT NULL',
            'text_prewiev'=>'VARCHAR(255) NOT NULL',
            'status'=>'INT(1) DEFAULT 2',
            'tags'=>'text NOT NULL',
            'path_img'=>'VARCHAR(255) DEFAULT NULL',
            'create_time'=>'INT(10) DEFAULT 0',
            'update_time'=>'INT(10) DEFAULT 0',
        ));
    }

    public function safeDown()
    {
        $this->dropTable("post");
    }
    
}
