<?php

namespace app\modules\blog\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\BaseStringHelper;
/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $text_post
 * @property integer $status
 * @property string $tags
 * @property integer $author_id
 * @property integer $create_time
 * @property integer $update_time
 */
class Post extends \yii\db\ActiveRecord
{
    public $string;
    public $image;
    public $filename;
    const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;
    const STATUS_ARCHIVED=3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text_post', 'tags'], 'required'],
            [['text_post', 'tags','path_img'], 'string'],
            [['create_time', 'update_time'], 'integer'],
            [['status'], 'in', 'range'=>[1,2,3]],
            [['title','text_prewiev'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text_post' => 'Text Post',
            'status' => 'Status',
            'tags' => 'Tags',
            'path_img' =>'Image',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }
    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->create_time=time();
            $this->update_time=time();
            $this->text_prewiev=BaseStringHelper::truncate($this->text_post,250,'...');
            $this->string = substr(uniqid('img'), 0,12);
            $this->image=UploadedFile::getInstance($this,'path_img');
            if ($this->image) {
                $this->filename='files/images/'.$this->string.'.'.$this->image->extension;
                $this->image->saveAs($this->filename);
                $this->path_img='/'.$this->filename;    
            }
            
        } else {
            $this->update_time=time();
            $this->text_prewiev=BaseStringHelper::truncate($this->text_post,250,'...');
            $this->image=UploadedFile::getInstance($this,'path_img');
            if ($this->image) {
                $this->image->saveAs(substr($this->path_img, 1));
            }
        }

        return parent::beforeSave($insert);
    }
}
