<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\blog\models\Lookup;
/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="panel panel-default">
      <div class="panel-heading"><?= $model->title?></div>
      <div class="panel-body">
           <img src="<?= $model->path_img ?>" alt="..." >
           <?= Html::decode($model->text_post);  ?>

      </div>
      <div class="panel-heading"> <?= $model->getAttributeLabel('status'); ?>:<?= Lookup::item('postStatus',$model->status) ?></div>
      <div class="panel-heading"> <?= $model->getAttributeLabel('create_time'); ?>:<?= Yii::$app->formatter->asDate($model['create_time'], 'd MMMM yyyy') ?></div>
      <?php if ($model->update_time) { ?>
            <div class="panel-heading"> <?= $model->getAttributeLabel('update_time'); ?>:<?= Yii::$app->formatter->asDate($model['update_time'], 'd MMMM yyyy') ?></div>
        <?php }  ?> 
    </div>
    
</div>
