<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\blog\models\Lookup;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\blog\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

   <div class="row">
   <?php foreach ($posts as $value) { ?>
       
        <div class="col-sm-12 col-md-12">
    <div class="thumbnail">
      <img class="img-posts" src="<?= $value->path_img ?>" alt="..." >
      
      <div class="caption">
        <h3><?= $value->title ?></h3>
        <p><?= $value->text_prewiev ?></p>
        <p><?= Html::a('Open', ['view', 'id' => $value->id], ['class' => 'btn btn-primary']) ?>
        <p> <?= $value->getAttributeLabel('status'); ?>:<?= Lookup::item('postStatus',$value->status) ?></p>
        <p> <?= $value->getAttributeLabel('create_time'); ?>:<?= Yii::$app->formatter->asDate($value['create_time'], 'd MMMM yyyy') ?></p>
        <?php if ($value->update_time!=$value->create_time) { ?>
            <p><?=  $value->getAttributeLabel('update_time'); ?>:<?= Yii::$app->formatter->asDate($value['update_time'], 'd MMMM yyyy') ?></p>
        <?php }  ?>
      </div>
      <div class="clear"></div>
    </div>
  </div>


  <?php  } ?>
    </div>

    
</div>

<?= 
 \yii\widgets\LinkPager::widget([
        'pagination'=>$pages,
]); ?>

